Carregar index
```
python3 IndexFiles.py --index abs --path arxiv_abs
```

# Scripts pels experiments
Exemple:
```
./scripts/clusters.sh
./scripts/freq.sh
./scripts/freq2.sh
./scripts/size.sh
```

## Scripts kmeans
```
./scripts/kmeans.sh -n freq
./scripts/kmeans.sh -n size
./scripts/kmeans.sh -n freq2
```

```
./scripts/kmeans_cluster.sh
```

## Script processar results
Processarà tots els resultats dels experiments
```
./scripts/results.sh
```
## Executar tots els experiments alhora
```
./scripts/kmeans_all.sh
```
# Resultats experiments
Els resultats dels experiments es troben en les carpetes KmeansClusters, Kmeansfreq, Kmeansfreq2, Kmeanssize


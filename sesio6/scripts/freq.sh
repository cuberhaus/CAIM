m_freq=(0.3 0.5 0.7 0.9 1)
n=${#m_freq[@]}
trap 'trap " " SIGTERM; kill 0; wait; cleanup' SIGINT SIGTERM
for ((i = 0; i < n; i++)); do
    freq=${m_freq[$i]}
    # The & has to be put outside the subshell otherwise the wait won't work properly because it is not directly
    # created by the current shell
    (
        set -x
        python3 ExtractData.py --index abs --minfreq 0.1 --maxfreq "$freq" --numwords 200 --name "$i"
    ) &
done
wait # This will wait for all child tasks to finish

for ((i = 0; i < n; i++)); do
    (
        set -x
        python3 GeneratePrototypes.py --data documents"$i".txt --nclust 8
    ) &
done
wait

mkdir -p experiments/freq
mv *.txt experiments/freq/

wait
me=$(basename "$0")
echo "${me} ended successfully"

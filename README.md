# CAIM
This repository contains code and materials for the lab sessions in the course "Cerca i Anàlisi de la Informació" (Search and Analysis of Information), which covers various topics in information retrieval and natural language processing.

## Contents
The following lab sessions are included in this repository:

- PageRank: implementation of the PageRank algorithm for ranking web pages based on the structure of hyperlinks between them.
- Zipf law: analysis of the frequency distribution of words in a text corpus, and visualization of the Zipf law.
- TF-IDF: implementation of the TF-IDF (term frequency-inverse document frequency) weighting scheme for text retrieval.
- Rocchio: implementation of the Rocchio algorithm for relevance feedback in text retrieval.
- MapReduce: implementation of the MapReduce programming model for processing large-scale data sets.
- iGraph: introduction to the iGraph library for network analysis and visualization.
Each lab session is contained in its own directory, which includes a file with instructions and explanations, as well as any necessary code and data files.

## Requirements
To run the code in this repository, you will need:

- Python 3.6 or higher
- Jupyter Notebook (for some lab sessions)
- The following Python libraries: numpy, pandas, matplotlib, scipy, scikit-learn, networkx, igraph, mrjob
